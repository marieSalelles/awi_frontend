
# Twiddle Web Application

## Presentation

This repository is the deliverable front-end of the Twiddle Application.
You can find the back-end repository at this link: https://gitlab.com/ysanson/awi_backend
Twiddle is a web plateform meant to gather teaching video content and to facilitate the exchange between teachers and students.

The main need is to provide efficient learning and a different way to learn through the implementation of the video transcript. We should be able to synchronize both at the same time to follow the video with the text. A user can go to a certain position in the video by clicking on the desired passage on the text. 
Moreover, another need is to provide a live stream to diffuse a conference or course.

**This project has been aborted and this application is just a prototype.**

### Installation to develop

Pre-requisite : GIT, react, node.js and npm

Open a terminal and hit the following command at the folder you want to clone the project to:
```bash
git clone https://gitlab.com/marieSalelles/awi_frontend.git
```

Then use the following command on the root of your project file to install the dependencies and start the developement server:

```bash
npm i
npm start
```
Please read the Available Scripts down below for more information about it.

## Technology choices 

We chose to use NodeJS for the back-end, React with Redux for the front-end and MongoDB for the database.  

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Credits
This project has been made by Maria Clara Machry Jacintho, Marie Salelles, Yvan Sanson et Nathan Traineau
