import {combineReducers} from 'redux'
import courses from './coursesReducer'

const allReducers = combineReducers({
    courses : courses
})

export default allReducers