import {FETCH_COURSES, FETCH_COURSES_FAILURE, ADD_COURSE,  ADD_COURSE_FAILURE} from '../actions/coursesAction';

export default function coursesReducer(state = [], action) {
    switch (action.type) {
        case ADD_COURSE:
            return [...state, action.payload];
        case FETCH_COURSES:
            return action.courses;
        case FETCH_COURSES_FAILURE:
            return action.error;
        case ADD_COURSE_FAILURE:
            return action.error;
        default:
            return state;
    }
}