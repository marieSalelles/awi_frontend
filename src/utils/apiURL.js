import "dotenv/config"

const apiURL = process.env.REACT_API_URL || "https://twiddle-back.igpolytech.fr/api"

export default apiURL;
