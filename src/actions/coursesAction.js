import axios from 'axios';
import apiUrl from '../utils/apiURL';

export const FETCH_COURSES= 'FETCH_COURSES';
export const FETCH_COURSES_FAILURE = 'FETCH_COURSES_FAILURE';
export const ADD_COURSE = 'ADD_COURSE';
export const ADD_COURSE_FAILURE = 'ADD_COURSE_FAILURE';

export const createCourse = ({ userId, name , desc, img, synchro, driveFolder, access, password, code }) => {
    return async (dispatch) => {
        try {
            const response = await axios.post(`${apiUrl}/course`, 
                {   userId: userId,
                    courseName: name,
                    description: desc,
                    courseImage: img,
                    synchro: synchro,
                    driveFolder: driveFolder,
                    access: access,
                    password: password,
                    code: code });
            dispatch(createCourseSuccess(response.data));
        }
        catch (error) {
            dispatch(createCourseFailure(error));
        }
    };
};

export const createCourseSuccess =  (data) => {
    return {
        type: ADD_COURSE,
        payload: {
            _id: data._id,
            creatorId: data.creatorId,
            courseName: data.courseName,
            description: data.description,
            image: data.image,
            access: data.access,
            password: data.password,
            isSynchro: data.isSynchro,
            driveFolder: data.driveFolder,
            videos: data.videos
        }
    }
};

export const createCourseFailure =  (error) => {
    return {
        type: ADD_COURSE_FAILURE,
        payload: {
            error: error
        }
    }
};

export const fetchPostsByCreator = (userId) => {
    return async (dispatch) => {
        try {
            const response = await axios.get(`${apiUrl}/courses/${userId}`);
            dispatch(fetchCourses(response.data));
        }
        catch (error) {
            dispatch(fetchCoursesFailure(error));
        }
    };
};

export const fetchPostsByUSer = (userId) => {
    return async (dispatch) => {
        try {
            const response = await axios.get(apiUrl);
            dispatch(fetchCourses(response.data));
        }
        catch (error) {
            dispatch(fetchCoursesFailure(error));
        }
    };
};

export const fetchCourses = (courses) => {
    return {
        type: FETCH_COURSES,
        courses
    }
};

export const fetchCoursesFailure = (error) => {
    return {
        type: FETCH_COURSES_FAILURE,
        error
    }
};
