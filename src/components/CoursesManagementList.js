import React from 'react'
import CourseItem from './CourseManagementItem'

const CoursesList = () => (
  <div className="courseList">
    <ul id="courseMList">
      <CourseItem></CourseItem>
      <CourseItem></CourseItem>
      <CourseItem></CourseItem>
    </ul>
  </div>
);

export default CoursesList
