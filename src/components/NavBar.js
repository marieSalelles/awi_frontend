import React from 'react'
import { Navbar, Nav, NavDropdown, Form, FormControl, Button } from 'react-bootstrap'
import '../css/VerticalNav.css'

const NavBar = () => ( 
<Navbar id="mainNav" fixed="top" bg="dark" variant="dark" expand="lg">
  <Navbar.Brand href="/">Twiddle</Navbar.Brand>
  <Navbar.Toggle aria-controls="basic-navbar-nav" />
  <Navbar.Collapse id="basic-navbar-nav">
    <Nav className="mr-auto">
      <Nav.Link href="/">Home</Nav.Link>
      <NavDropdown title="Profile" id="basic-nav-dropdown">
        <NavDropdown.Item href="#action/2.1">My profile</NavDropdown.Item>
        <NavDropdown.Item href="#action/2.2">My Dashboard</NavDropdown.Item>
        <NavDropdown.Divider />
        <NavDropdown.Item href="/1/courses">Course Management</NavDropdown.Item>
      </NavDropdown>
      <Nav.Link href="#link">Live Stream</Nav.Link>
    </Nav>
    <Form inline>
      <FormControl type="text" placeholder="Search courses" className="mr-sm-2" />
      <Button variant="outline-info">Search</Button>
    </Form>
  </Navbar.Collapse>
</Navbar>
)

export default NavBar
