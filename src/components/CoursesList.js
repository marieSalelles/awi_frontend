import React from 'react'
import Course from './CourseItem'
import '../css/CourseList.css'
import ItemsCarousel from 'react-items-carousel';
import range from 'lodash/range';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleLeft, faAngleRight} from '@fortawesome/free-solid-svg-icons'

class CourseList extends React.Component {

    componentWillMount() {
      this.setState({
        children: [],
        activeItemIndex: 0,
      });

      setTimeout(() => {
        this.setState({
          children: range(20).map(i => <Course key={i}></Course>)
        })
      }, 100)
    }

    changeActiveItem = (activeItemIndex) => this.setState({ activeItemIndex });

    render() {
      const {
        activeItemIndex,
        children,
      } = this.state;

      return (
        <div className="courseList">
        <h2 className="listTitle">List name</h2>
            <ItemsCarousel
            // Placeholder configurations
            enablePlaceholder
            numberOfPlaceholderItems={5}
            minimumPlaceholderTime={1000}
            placeholderItem={<div style={{ height: 200, background: 'none' }}>CourseName</div>}

            // Carousel configurations
            numberOfCards={3}
            gutter={12}
            showSlither={true}
            firstAndLastGutter={true}
            freeScrolling={false}

            // Active item configurations
            requestToChangeActive={this.changeActiveItem}
            activeItemIndex={activeItemIndex}
            activePosition={'center'}

            chevronWidth={24}
            rightChevron={<FontAwesomeIcon icon={faAngleRight} size="2x" />}
            leftChevron={<FontAwesomeIcon icon={faAngleLeft} size="2x" />}
            outsideChevron={false}
            >
            {children}
            </ItemsCarousel>
        </div>
    );
    }
  }

export default CourseList
