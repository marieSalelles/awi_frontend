import React from 'react'
// import {Form, Button} from 'react-bootstrap'
import NavBar from './NavBar'
import FormCourse from '../containers/CreateCourse'

const NewCoursePage = () => (
    <div id="addCoursePage">
        <NavBar/>
        <div className="container newCourse">
            <h3>Add a new course</h3>
            <FormCourse/>
        </div>
    </div>
)

export default NewCoursePage
