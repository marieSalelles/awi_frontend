import React from 'react'
import logo from '../logo.svg'
import '../css/CourseList.css'

const CourseItem = () => (
    <li className="courseItem">
        <a className="courseLink" href="/course/1">
            <img className="courseImg" src={logo} alt="The course logo"/>
            <h3 className="courseName">Course name</h3>
        </a>
    </li>
)

export default CourseItem
