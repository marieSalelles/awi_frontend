import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars, faTimes } from '@fortawesome/free-solid-svg-icons'
import '../css/CollapsibleBox.css'
import SideBar from './VerticalNav'

class Collapsible extends React.Component {
    constructor(){
        super();
        this.state = {
            open: true,
            icon: faTimes
        };
        this.togglePanel = this.togglePanel.bind(this);
    }

    togglePanel(e){
        let newIcon;
        if (this.state.icon === faTimes) {
            newIcon = faBars;
        }
        else newIcon  = faTimes;
        this.setState({open: !this.state.open, icon: newIcon })
    }

  render () {
    return (<div className="LeftCollapsibleBox float-left">
      <div onClick={(e) => this.togglePanel(e)} className="headerLCollapsibleBox">
        <FontAwesomeIcon icon={this.state.icon} size="2x" />
      </div>
      {this.state.open ? (
        <div className="content">
            <h3>Menu title</h3>
            <SideBar/>
        </div>
      ) : null}
    </div>)
  }
}

export default Collapsible
