import React from 'react'
import NavBar from './NavBar';
// import { Link } from 'react-router-dom'
import {Button} from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus } from '@fortawesome/free-solid-svg-icons'
import '../css/CoursesManagementPage.css'
import CoursesList from './CoursesManagementList'

const CoursesManagement = () => (
    <div className="courseManagementPage">
        <NavBar/>
        <div className="container">
            <h1 >Courses Management Page</h1>
            <div id="newCourse">
                <Button href="/addcourse" variant="primary">
                    <FontAwesomeIcon icon={faPlus} size="1x" />
                    Add a course
                </Button>
            </div>
            <CoursesList/>
        </div>
    </div>
)

/* <ul>
            <li>
            <Link to="/">Home</Link>
            </li>
        </ul> */
export default CoursesManagement
