import React from 'react'
import '../css/VideoContainer.css'

const VideoContainer = () => (
    <div className="flexible-container">
        <iframe title="video" />
    </div>
)

export default VideoContainer
