import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleLeft, faAngleRight } from '@fortawesome/free-solid-svg-icons'
import '../css/CollapsibleBox.css'

class Collapsible extends React.Component {
    constructor(){
        super();
        this.state = {
            open: true,
            icon: faAngleRight
        };
        this.togglePanel = this.togglePanel.bind(this);
    }
   togglePanel(e){
        let newIcon;
        if (this.state.icon === faAngleRight) {
            newIcon = faAngleLeft;
        }
        else newIcon = faAngleRight
        this.setState({open: !this.state.open, icon: newIcon })
    }

  render (){
    return (<div className="RightCollapsibleBox float-right">
      <div onClick={(e) => this.togglePanel(e)} className="headerRCollapsibleBox">
        <FontAwesomeIcon icon={this.state.icon} size="2x" />
      </div>
      {this.state.open ? (
        <div className="content">
          <h3>Transcript</h3>
          <p>Video transcript</p>
        </div>
      ) : null}
    </div>)
  }
}

export default Collapsible
