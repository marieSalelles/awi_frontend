import React from 'react'
import '../css/App.css'
import NavBar from './NavBar'
import SideBar from './LeftCollapsibleBox'
import CourseList from './CoursesList'
import 'bootstrap/dist/css/bootstrap.min.css'

function App () {
  return (
    <div className="App">
      <NavBar></NavBar>
      <SideBar></SideBar>
      <div className=" mainDiv col-xl-7 col-md-8 col-10">
        <CourseList></CourseList>
        <CourseList></CourseList>
        <CourseList></CourseList>
        <CourseList></CourseList>
        <CourseList></CourseList>
      </div>
    </div>
  )
}

export default App
