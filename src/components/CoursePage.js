import React from 'react'
import MainNavBar from './NavBar'
import LCollapseBox from './LeftCollapsibleBox'
import RCollapseBox from './RightCollapsibleBox'
import Video from './VideoContainer'

const Course = () => (
  <div className="CourseManagementPage">
    <MainNavBar></MainNavBar>
    <LCollapseBox></LCollapseBox>
    <Video></Video>
    <RCollapseBox></RCollapseBox>
  </div>
)

export default Course
