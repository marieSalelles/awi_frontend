import React from 'react'
import logo from '../logo.svg'
import '../css/CoursesManagementPage.css'
import { Button, ButtonGroup } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChartBar, faKey } from '@fortawesome/free-solid-svg-icons'

const CourseItem = () => (
        <li className="courseMItem">
            <div className="courseDiv col-xl-4 col-md-7 col-12">
                <a className="courseManagementLink" href="/">
                    <div className= "courseInfos">
                        <h3 className="courseName">Course name</h3>
                        <p className="NumberVideos"> 20 videos</p>
                    </div>
                    <img className="courseImage" src={logo} alt="Logo"/>
                </a>
            </div>
        <ButtonGroup vertical className="buttonGroupManagement col-xl-4 col-md-5 col-12">
          <Button href="#" variant="info" className="managementCourseButton">
            <FontAwesomeIcon icon={faChartBar} size="1x" />
                    Course dashboard
          </Button>
          <Button href="#" variant="warning" className="managementCourseButton">
            <FontAwesomeIcon icon={faKey} size="1x" />
                    Update the password
          </Button>
        </ButtonGroup>
      </li>
);

export default CourseItem
