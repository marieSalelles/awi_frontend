import React from 'react'
import { Nav } from 'react-bootstrap'

const SideBar = () => (
  <Nav className="flex-column">
    <Nav.Item >
      <Nav.Link className="sideBarItem" href="/">Active</Nav.Link>
    </Nav.Item>
    <Nav.Item >
      <Nav.Link className="sideBarItem" href="/">Link</Nav.Link>
    </Nav.Item>
  </Nav>
)

export default SideBar
