import React from 'react'
import { Form, Button } from 'react-bootstrap'
import '../css/AddCoursePage.css'

class CourseForm extends React.Component{

  constructor(props){
    super(props);
    this.state = {
        userId: 1,
        name: '',
        desc: '',
        img: '',
        synchro: false,
        driveFolder: '',
        access: true,
        password:'',
        code:''
    }
    this.handleSwitchChange = this.handleSwitchChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.handleAccess = this.handleAccess.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleSwitchChange(e, synchro) {
    this.setState({
      synchro: synchro
    });
  }

  handleInputChange (e) {
      this.setState({
          [e.target.name]: e.target.value
      });
  };

  handleSubmit(e){
      e.preventDefault();
      if (this.state.name !== '') {
          this.props.addCourse(this.state);
          this.handleReset();
      }
  }

  handleReset(){
      this.setState({
          userId: '',
          name: '',
          desc: '',
          img: '',
          synchro: false,
          driveFolder: '',
          access: true,
          password:'',
          code:''
      })
  }

  handleAccess(e, access){
      this.setState({
          access: access
      });
  }

  render() {
    return (
      <Form id="newCourseForm" onSubmit={ this.handleSubmit }>
        <Form.Group controlId="formCourseName">
          <Form.Label>Course name</Form.Label>
          <Form.Control type="text" name="name" placeholder="Enter a name" value={this.state.name} onChange={ this.handleInputChange }/>
        </Form.Group>

        <Form.Group controlId="formCourseDescription">
          <Form.Label>Give a course description</Form.Label>
          <Form.Control name="desc" as="textarea" rows="3" value={this.state.desc} onChange={ this.handleInputChange }/>
        </Form.Group>

        <Form.Group controlId="formCourseImage">
          <Form.Label>Upload a course image</Form.Label>
          <div className="input-group">
            <div className="input-group-prepend">
              <span className="input-group-text" id="inputGroupFileAddon01">
                Upload
              </span>
            </div>
            <div className="custom-file">
              <input
                type="file"
                name="img"
                accept="image/*"
                className="custom-file-input"
                id="inputGroupFile01"
                aria-describedby="inputGroupFileAddon01"
                onChange={ this.handleInputChange }
              />
              <label className="custom-file-label" htmlFor="inputGroupFile01">
                Choose an image
              </label>
            </div>
          </div>
        </Form.Group>

        <Form.Group controlId="formSynchronizedCourse">
          <Form.Label>Synchronized the course content.</Form.Label>
          <Form.Check
            type="switch"
            id="custom-switch"
            label="Synchronized with a Google Drive folder"
            onClick={(e)=>this.handleSwitchChange(e, !this.state.synchro)}
          />
           {this.state.synchro ? (
            <Form.Control type="text" name="driveFolder" placeholder="Enter the Google Drive folder link"  value={this.state.driveFolder} onChange={ this.handleInputChange }/>)
            :null}
        </Form.Group>

        <Form.Group controlId="formCourseaccess">
          <Form.Label>Give the type of access for the course.</Form.Label>
          <Form.Check
            name="acces"
            type="radio"
            id="PrivateCourse"
            label= "Private access"
            onClick={(e)=>this.handleAccess(e, false)}
          />
          <Form.Check
            name="acces"
            type="radio"
            id="PublicCourse"
            label= "Public access"
            onClick={(e)=>this.handleAccess(e, true)}
          />
          {!this.state.access ? (
            <div>
              <Form.Label>Give a password to see the course content.</Form.Label>
              <Form.Control name="password" type="password" placeholder="Enter a password." value={this.state.password} onChange={ this.handleInputChange }/>
            </div>
          ) : null}
        </Form.Group>

        <Button variant="primary" type="submit">
          Submit
        </Button>
      </Form>
    )
  }
}


export default CourseForm
