import React from 'react';
import ReactDOM from 'react-dom';
import thunk from 'redux-thunk';
import './css/index.css';
import { Route, BrowserRouter as Router, Switch  } from 'react-router-dom'
import App from './components/App';
import CourseManagement from './components/CoursesManagementPage'
import Course from './components/CoursePage'
import AddCourse from './components/AddCoursePage'
import NotfoundPage from './components/NotFound'
import * as serviceWorker from './serviceWorker'
import {Provider} from 'react-redux'
import {createStore, applyMiddleware} from 'redux'
import allReducers from './reducers'


const store = createStore(allReducers, applyMiddleware(thunk));


const routing = (
    <Provider store = {store}>
        <Router>
            <div>
                <Switch>
                    <Route path="/:userId/courses" component={CourseManagement} />
                    <Route path="/course/:id" component={Course} />
                    <Route path="/addcourse" component={AddCourse} />
                    <Route exact path="/" component={App} />
                    <Route component={NotfoundPage} />
                </Switch>
            </div>
        </Router>
    </Provider>
  )

ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
