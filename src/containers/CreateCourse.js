import { connect } from 'react-redux';
import { createCourse } from '../actions/coursesAction'
import AddCourseFrom from '../components/AddCourseForm'

const mapDispatchToProps = dispatch => {
    return {
        addCourse: course => {
            dispatch(createCourse(course));
        }
    };
};

export default connect(
    null,
    mapDispatchToProps
)(AddCourseFrom);