# stage 1: build
FROM node:12 as react-build
WORKDIR /app
COPY . ./
RUN npm i
RUN npm run build

# stage: 2 — the production environment
FROM nginx:stable-alpine

RUN rm -rf /etc/nginx/conf.d
COPY conf /etc/nginx
COPY --from=react-build /app/build /usr/share/nginx/html

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
